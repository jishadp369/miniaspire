<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Hash;
use Auth;

class UserController
{
    public function create(Request $request){
      $validator = Validator::make($request->all(), [
        'full_name'    => 'required',
        'role'  => 'required',
        'email'  => 'required|unique:users',
        'password'  => 'required',
        'heard_through'  => 'required',
      ]);
      if ($validator->fails()) {
         return ['status'=>300,'message'=>'validation error','data'=>$validator->errors()];
      }
      User::create([
        'full_name'  =>request('full_name'),
        'role'  =>request('role'),
        'email'  =>request('email'),
        'password'  =>Hash::make(request('password')),
        'heard_through'  =>request('heard_through'),
      ]);

      return ['status'=>200,'message'=>'Admin Created Succesfully'];
    }

    public function login(Request $request){
      $validator = Validator::make($request->all(), [
        'email'    => 'required',
        'password'    => 'required',
      ]);

      if ($validator->fails()) {
         return ['status'=>300,'message'=>'validation error','data'=>$validator->errors()];
      }
      $inputAuth  = ['email'=>request('email'),'password'=>request('password')];
      if(Auth::guard('user')->attempt($inputAuth)){
        $user = Auth::guard('user')->user();
        $token = $user->createToken('mobile-app')->accessToken;  //CREATING TOKEN
        return ['status'=>200,'message'=>'login success','data'=>['token'=>'Bearer '.$token]];
      }else{
        return ['status'=>300,'message'=>'login failed'];
      }
    }
}
