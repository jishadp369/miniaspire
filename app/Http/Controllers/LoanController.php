<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\UserLoan;
use App\Models\UserLoanPayment;
use Auth;

class LoanController
{
    public function newLoan(){
      $validator = Validator::make(request()->all(), [
        'amount'    => 'required',
        'duration'    => 'required',
        'repayment_frequency'  => 'required',
        'interest_rate'  => 'required',
        'arrangement_fee'  => 'required'
      ]);
      if ($validator->fails()) {
         return ['status'=>300,'message'=>'validation error','data'=>$validator->errors()];
      }
      UserLoan::create([
        'user_id' =>Auth::user()->user_id,
        'amount' =>request('amount'),
        'duration' =>request('duration'),
        'repayment_frequency' =>request('repayment_frequency'),
        'interest_rate' =>request('interest_rate'),
        'arrangement_fee' =>request('arrangement_fee'),
      ]);

      return ['status'=>200,'message'=>'Loan Created Succesfully'];
    }

    public function getActiveLoans(){
        $loans = UserLoan::where('user_id',Auth::user()->user_id)->where('status',1)->get();
        return ['status'=>200,'data'=>$loans];
    }

    public function loanRepayment(){
      $validator = Validator::make(request()->all(), [
        'user_loan_id'    => 'required|exists:user_loans',  //CHECKING LOAN ESIST OR NOT
        'due_month'  => 'required|numeric|min:1|max:12',
        'amount'  => 'required'
      ]);
      if ($validator->fails()) {
         return ['status'=>300,'message'=>'validation error','data'=>$validator->errors()];
      }
      if(UserLoan::find(request('user_loan_id'))->status ==1){  // CHECKING THE LOAN IS COMPLETED OR ACTIVE
        $alreadyDone = UserLoanPayment::where('due_month',request('due_month'))->count();
        if($alreadyDone==0){  //CHECKING THE PAYMENT RECIEVED OR NOT FOR THIS MONTH
          UserLoanPayment::create([
            'user_loan_id'  =>request('user_loan_id'),
            'due_month'  =>request('due_month'),
            'amount'  =>request('amount'),
          ]);
          // Checking payment reached total amount
          $paymentSum = UserLoanPayment::where('user_loan_id',request('user_loan_id'))
                      ->sum('amount');
          $userLoan = UserLoan::find(request('user_loan_id'));
          $loanAmount = $userLoan->amount+($userLoan->interest_rate*$userLoan->duration)+$userLoan->arrangement_fee;
          if($paymentSum >= $loanAmount){
            $userLoan->status = 0;
            $userLoan->save();
          }
          // Checking payment reached total amount
          return ['status'=>200,'message'=>'Loan Payment Recorded'];
        }else{
          return ['status'=>300,'message'=>'Loan Payment Already Completed'];
        }
      }else{
        return ['status'=>300,'message'=>'Your Loan is closed'];
      }


    }
}
