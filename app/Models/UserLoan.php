<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLoan extends Model
{
    protected $table = 'user_loans';
    protected $primaryKey = 'user_loan_id';
    public $fillable = ['user_id','amount','duration', 'repayment_frequency', 'interest_rate','arrangement_fee','status'];
}
