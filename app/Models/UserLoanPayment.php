<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLoanPayment extends Model
{
    protected $table = 'user_loan_repayments';
    protected $primaryKey = 'user_loan_repayment_id';
    public $fillable = ['user_loan_id', 'due_month','amount'];
}
