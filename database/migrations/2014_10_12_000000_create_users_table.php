<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('full_name');
            $table->tinyInteger('role')->comment("1:Company director,2:Loan broker");
            $table->string('email')->unique();
            $table->string('password');
            $table->tinyInteger('heard_through')->comment('1:Facebook, 2:Google,3:Brokers,4:Friends & Relatives,5:Brochure, 6:Online articles, 7:Email Invite, 8: Others');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
