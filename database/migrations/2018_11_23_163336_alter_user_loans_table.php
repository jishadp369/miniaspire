<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_loans', function (Blueprint $table) {
            $table->double('amount',15,3)->after('user_id');
            $table->tinyInteger('status')->after('arrangement_fee')->default(1)->comment('1:Active,0:Completed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_loans', function (Blueprint $table) {
            $table->dropColumn([
              'amount',
              'status',
            ]);
        });
    }
}
