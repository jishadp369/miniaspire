<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserLoanRepaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_loan_repayments', function (Blueprint $table) {
            $table->increments('user_loan_repayment_id');
            $table->unsignedInteger('user_loan_id');
            $table->foreign('user_loan_id')->references('user_loan_id')->on('user_loans')->onDelete('cascade');
            $table->tinyInteger('due_month')->comment('Month in PHP standard');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_loan_repayments');
    }
}
