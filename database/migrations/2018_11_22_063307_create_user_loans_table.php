<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_loans', function (Blueprint $table) {
            $table->increments('user_loan_id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('cascade');
            $table->smallInteger('duration')->comments('in months')->default(1);
            $table->tinyInteger('repayment_frequency')
                  ->comment("1:Every Month, 2:Every Two Months,3:Every 4 Months, 4:Every 6 Six Months")
                  ->default(1);
            $table->double('interest_rate',15,3);
            $table->double('arrangement_fee',15,3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_loans');
    }
}
