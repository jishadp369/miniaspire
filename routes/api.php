<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('new-login','UserController@create');
Route::post('login','UserController@login');

Route::group(['middleware'=>'auth:api'],function(){
  Route::post('new-loan','LoanController@newLoan');
  Route::post('get-user-loans','LoanController@getActiveLoans');
  Route::post('loan-repayment','LoanController@loanRepayment');
});
